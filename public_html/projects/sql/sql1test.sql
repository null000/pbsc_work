SELECT * FROM `users`;
SELECT * FROM `users` WHERE state = "FL";
SELECT first_name FROM `users` WHERE state = "FL";
SELECT * FROM `users` WHERE state != "FL";
SELECT * FROM `users` WHERE state = "FL" AND cars = 0;
SELECT * FROM `users` WHERE state = "FL" ORDER BY last_name;
SELECT * FROM `users` WHERE state IS null;
SELECT * FROM `users` ORDER BY opt_in_date DESC LIMIT 3;
-----------------------twoo

SELECT COUNT(id) FROM `users`;
SELECT COUNT(id) FROM `users` WHERE state   = "NY";
SELECT COUNT(id) FROM `users` WHERE opt_out = 1;
SELECT state, COUNT(id) FROM `users` GROUP BY state;
SELECT sum(cars) FROM `users`;
SELECT state, SUM(cars) FROM `users` WHERE state   = "FL" GROUP BY state;
SELECT MAX(cars) FROM `users`;
SELECT MIN(cars) FROM `users`;
SELECT last_name, SUM(cars) FROM `users` GROUP BY last_name ORDER BY SUM(cars) DESC;
SELECT last_name, state, SUM(cars) FROM `users` WHERE last_name="Smiths" GROUP BY last_name ORDER BY SUM(cars) DESC;
SELECT state, SUM(cars) FROM `users` WHERE last_name = "Smith" GROUP BY state HAVING SUM(cars) > 2;
