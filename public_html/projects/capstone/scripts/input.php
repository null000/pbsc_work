<script>
//input 
var listener = new window.keypress.Listener(
	document.getElementById("window"), 
	{
		is_unordered    : true,
		prevent_repeat  : true  
	}
);
//mouse input
canvas.addEventListener(
	//A pointing device is moved over an element. (Fired continously as the mouse moves.)
	'mousemove',
	function(event) {
		event.preventDefault();
		players[0].set_barrel_rotation(
			myTan2(
				(players[0].y - event.offsetY),
				(players[0].x - event.offsetX)
			)
		);
	}
);

canvas.addEventListener(
	//A pointing device is moved over an element. (Fired continously as the mouse moves.)
	'click',
	function(event) {
		event.preventDefault();
		players[0].fire();
	}
);
var my_scope = this;
[
	{
		player: 0,
		key:"w",
		idx:0,
		idy:0
	},
	{
		player: 0,
		key:"s",
		idx:1,
		idy:0
	},
	{
		player: 0,
		key:"e",
		idx:0,
		idy:1
	},
	{
		player: 0,
		key:"d",
		idx:1,
		idy:1
	},
].map(element => listener.register_combo(
    {
        "keys"          : element.key,
        "on_keydown"    : function(event) {
			event.preventDefault();
			players[element.player].steering[element.idx][element.idy]++;
			//console.log(players[element.player].steering[element.idx][element.idy] + "key "+element);
        },
        "on_keyup"      : function(event) {
			event.preventDefault();
			players[element.player].steering[element.idx][element.idy]--;
        },
        "this"          : my_scope
    }
));
//who needs variables
//do the mouse thing
</script>