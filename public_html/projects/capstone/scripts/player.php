<script>
	//players data	
	class Player {
		//hp is value * 3
		//speed is tbd is movement
		//dmg is flat damage to hp
	constructor(
		//position is the center of the object
		posX 			= 100,
		posY 			= 100,
		color 			= "#0000ff",
		speed 			= 8,
		hp 				= 4*3,
		damage,
		rotation 		= 0,
		reload_duration = 60,
		width			= 50,
		height 			= 50,
		barrel_rotation = 0,
		barrel_length 	= 70
		) {
			this.steering = [
				[0,0],
				[0,0]
			];
			this.width  = width;
			this.height = height;
			this.x 					= posX;
			this.y 					= posY;
			this.color 				= color;
			this.speed              = speed;
			this.hp 				= hp;
			this.damage 			= damage;
			this.rotation 			= rotation;
			this.reload_duration 	= reload_duration;
			this.width 				= width;
			this.height 			= height;			
			this.barrel_rotation 	= barrel_rotation;
			this.barrel_length 		= barrel_length;
			
			this.hypot 				= (this.speed * tank_speed);
			this.last_fire 			= -reload_duration;
			this.id 				= players.length;
			this.radius 			= pythag(this.height, this.width);
			//my distaste for java script grows stronger
			this.setColor = function() {		
				context.fillStyle = this.color;		
			}
			this.max_hp = hp;
		}
		
		set_barrel_rotation(arr = 0) {
			this.barrel_rotation = arr;
		}
		
		fire() {
			if(this.last_fire < game_clock) {
				this.last_fire = game_clock+this.reload_duration;
				bullets.push(
					new Bullet(
						this.x+Math.sin(this.barrel_rotation*Math.PI/180)*this.barrel_length,
						this.y-Math.cos(this.barrel_rotation*Math.PI/180)*this.barrel_length,
						this.barrel_rotation,
						"red",
						this.damage
					)
				);
			}
			else {
				//somthing
				var fart = "fart";
			}
		}
		//have player slide around drifty like
		move() {		
			
			/* 
				s=o/h
				c=a/h
				t=o/a
				 ____
				|   /
				|  /
				| /
				|/
				^
				theta
			*/
			
			
			//forward
			if(this.steering[0][0] > 0 && this.steering[0][1] > 0) {				
				this.y -= Math.cos(this.rotation*Math.PI/180) * this.hypot;				
				this.x += Math.sin(this.rotation*Math.PI/180) * this.hypot;
			}
			//backward
			if(this.steering[1][0] > 0 && this.steering[1][1] > 0) {
				this.y += Math.cos(this.rotation*Math.PI/180) * this.hypot;				
				this.x -= Math.sin(this.rotation*Math.PI/180) * this.hypot;
			}
			
			
			
			var rotation_factor = .6;
			//rotate rightweeeee
			if(
				this.steering[0][0] > 0 &&
				this.steering[1][1] > 0
			) {				
				this.rotation = (this.rotation+this.speed*rotation_factor+360) % 360;
				//console.log("current rotation " + this.rotation);
			}
			//rotate left
			if(
				this.steering[0][1] > 0 &&
				this.steering[1][0] > 0
			) {
				this.rotation = (this.rotation-this.speed*rotation_factor+360) % 360;
			}
			if(this.x < 0) this.x = 0;
			if(this.y < 0) this.y = 0;
			
			if(this.x > <?= $width  ?>) this.x = <?= $width ?>;			
			if(this.y > <?= $height ?>) this.y = <?= $height ?>;
		}
		
		draw() {
			context.translate(this.x, this.y)
			context.rotate(this.rotation*Math.PI/180);
			//body
			this.setColor();
			context.fillRect(this.width/-2,this.height/-2,this.width,this.height);
			
			//bumbper thing
			context.fillStyle = "red";
			context.fillRect(-this.width*.4, -this.height*.5, this.width*.8, this.height*.2);
			//barrel
			context.fillStyle = "green";			
			
			context.rotate((this.barrel_rotation*Math.PI/180)-this.rotation*Math.PI/180);			
			context.fillRect(-this.width*.1, -this.barrel_length, this.width*.2, this.barrel_length);
			
			context.rotate(-this.barrel_rotation*Math.PI/180);			
			
			//hp bar
			context.fillStyle = "rgba(0,255,0,0.4)";
			context.fillRect(-this.width * 1.1/2, -this.height * 0.9, this.width * 1.1, this.height * 0.1);
			context.fillStyle = "rgba(255,0,0,0.6)";
			context.fillRect(this.width * 1.1/2, -this.height * 0.9, (-this.width * 1.1)*(1-this.hp/this.max_hp), this.height * 0.1);
			context.translate(-this.x, -this.y);
		}
		
		collide_with_bullet(bill) {
			var debug = false;
			var hypot = pythag(this.x-bill.x, this.y-bill.y);			
			
			
			//if(debug) console.log(-this.rotation);
			var rotatedX = bill.x+Math.sin(-this.rotation*Math.PI/180)*hypot;
			var rotatedY = bill.y+Math.cos(-this.rotation*Math.PI/180)*hypot;
			if(debug) console.log("x"+rotatedX+":y"+rotatedY);
			//simplfied
			var anw = this.detect(bill, bill.x, bill.y);
			//----------------------todo make it better!!!!!!!!!!!!!!!
			//var anw = this.detect(bill, rotatedX, rotatedY);
			//console.log(this.detect(bill, bill.x, bill.y));
			//blog(bill.x+":"+bill.y);
			//blog(rotatedX+":"+rotatedY);
			if(anw) {
				this.hit(bill.damage);
				console.log(this.hp);
			}
			return anw;
		}
		
		detect(bill, rotatedX, rotatedY) {
			var anw = 
				((this.x 	- this.width/2 ) 	< (rotatedX + bill.radius	)) &&
				((rotatedX 	- bill.radius) 	< (this.x 	+ this.width/2	)) &&
				((this.y 	- this.width/2 ) 	< (rotatedY + bill.radius	)) &&
				((rotatedY 	- bill.radius) 	< (this.y 	+ this.height/2   ));
			return anw;
		}
		
		hit(incomingDamage = 0) {
			//todo flash the screen or something
			this.hp -= incomingDamage;
			if(this.hp <= 0) {
				death_count++;
				players[this.id] = null;
			}
		}
}
</script>