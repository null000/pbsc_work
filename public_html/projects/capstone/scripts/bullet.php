<script>
class Bullet {
	constructor(
		x,
		y,
		rotation = 0,
		color = "red",	
		damage = 1,
		radius = 10
	){
		this.x 		= x;
		this.y 		= y;
		this.setVelocity(rotation);
		this.color 	= color;
		this.radius = radius;
		this.damage = damage;
		this.id		= bullets.length;
		//console.log("bullet number " + this.id);
		this.birth 	= game_clock;
	}
	
	setVelocity(angle) {		
		this.velocityX = Math.sin(angle * Math.PI/180)*bullet_speed;
		this.velocityY = Math.cos(angle * Math.PI/180)*bullet_speed;
		if(false) console.log(
		"bulet x : " + this.velocityX +
		" bulet y : " + this.velocityY
		);
	}
	
	move() {
		this.x += this.velocityX;
		this.y -= this.velocityY;
	}
	
	draw() {
		context.fillStyle = this.color;
		context.beginPath();//collapse the wave function
		context.arc(this.x, this.y, this.radius, 0, 2 * Math.PI, false);
		context.fill();
		}
}
</script>