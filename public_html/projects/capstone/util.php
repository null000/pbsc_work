<?php
$tdCount = 0;

//accepts any number of arrays and string params
//todo make a class with it class html wrapper
function td() {
	$anw = T::d(func_get_args());
	T::setWrapping();
	return $anw;
}
class T {
private static	$wraping = "<td>";
private static	$slash = "/";

public  static function setWrapping($string1 = "<td>", $string2 = "/") {
	T::$wraping = $string1;
	T::$slash   = $string2;
}
public  static function setSlash($string = "/") {
	
}

public  static function d($string) {
	$GLOBALS["tdCount"]++;
	//base case
	if(func_num_args() == 1) {
		if(is_array($string)) {
			$anw = "";
			foreach($string as $k => $v) {
				$anw .= T::d($v);
			}
			return $anw;
		}
		else {
			//most base case
			return T::$wraping . $string . substr_replace(T::$wraping, T::$slash, 1, 0);
		}
	}
	else {
		$anw = "";
		foreach(func_get_args() as $k => $v) {
			$anw .= T::d($v);
		}
		return $anw;
	}
}
}
function tableInput($name, $id, $type = "text", $min = "0", $max="100", $value="") {
	?>
	<tr>
		<?= T::d($name,
			"<input ".
			'id="'.$id.'" '.
			"type=\"" . $type .'" '.
			"class=\"form-control\" placeholder=\"\" name=\"". $id."\"".
			'max="'.$max.'"'.
			'min="'.$min.'"'.
			'value="'.$value.'"'.
			">",
			'<span id="span'.$id.'"></span>'
			);
		?>
	</tr>
	<?php
}
?>