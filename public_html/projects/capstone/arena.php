<head>

	<style>
		#myCanvas {
			border: 5px solid black;
		}
		body {
			background-color: gray;
			text-align: center;
		}
	</style>
	<!-- --
	<script src="../../node_modules/gl-matrix/dist/gl-matrix-min.js" type="module"></script>
	-->
	<script src="keypress/keypress.js"></script>
	
</head>
<?php 
	//so dirty php doing the magic numbers
	$width = 1000;
	$height = 500;
	$playerCount = 1;
?>
<!-- 
<p>
	the controls are tank drive<br>
	w e<br>
	s d<br>
	hold w e to go forward<br>
	hold s d to go backward<br>
	hold w d to turn left<br>
	
</p>
-->
<canvas id="glCanvas" width="<?=$width?>" height="<?=$height?>">
</canvas>
<!-- 
	make html based hud
	current hp
	points
-->
<script>
//initialize all the things
	
	
	
  //===========================================================================
  // CONSTANTS
  //===========================================================================
var canvas 		= document.getElementById('glCanvas');
var context 	= canvas.getContext('2d');
var game_clock 	= 0;
	  
	  
var tick         = 1000/20;//how often the game updates itself every 17 ms is about 60 frames per secound
var bullet_speed = 4;
var tank_speed   = 60/100;//base speed is multiplied
var players = [];
</script><?php
include "db.php";
include "scripts/player.php";
include "scripts/input.php";
include "scripts/bullet.php";
include "scripts/crazy.php";
?><script>
<?php
	for($i = 0; $i < $playerCount; $i++) {
	echo "players.push(new Player());";
	}
?>

players[0].hp 		= <?= $_SESSION["hp"]?>;
players[0].speed	= <?= $_SESSION["speed"]?>;
players[0].damage	= <?= $_SESSION["damage"]?>;

<?php
for($i = 0; $i < 3; $i++) {
?>new crazy();<?php
}
?>
//---------------------------test 
var bullets = [];
var death_count= 0;
//game loop
main();

function main(){
	setTimeout(function() {	
		winConditions(players);	
		//detec collision
		collide(bullets, players);
		animate(players.concat(bullets), canvas, context);
		
		game_clock++;
		main();		
	}, tick);
}
function winConditions(players) {
	if(players[0] == null) window.location.href = ("lose.php");
	var enemy = players.length - <?= $playerCount ?>;
	var sum = 0;
	for(var i = 0; i < players.length; i++) {
		if(players[i] == null) sum++;		
	}
	if(sum >= enemy) window.location.href = ("win.php?points="+(240-game_clock/60)*enemy);
	//console.log("sum:"+sum+":enem:"+enemy);
}
function animate(gameObjects, canvas) {
        // update		
		context.clearRect(0, 0, canvas.width, canvas.height); 
		context.fillStyle ="black";
		context.fillRect(0, 0, canvas.width, canvas.height);
        for(g of gameObjects) {
			if(g != null && typeof(g) !== "undefined") {
			g.move();
			g.draw();
			}
		}		
}

function collide(bullets, tanks) {
	for(b of bullets) {
		if(b != null && typeof(b) !== "undefined") {
			//if(game_clock%60 == 0)console.log(b.id);
			if(
			0 > b.x-b.radius || b.x+b.radius > <?= $width  ?> || 
			0 > b.y-b.radius || b.y+b.radius > <?= $height ?> 			
			) {
				bullets[b.id] = null;
			}
			else {
				for(t of tanks) {					
					if(t != null && t.collide_with_bullet(b)) {
						console.log("collided");
						bullets[b.id] = null;
					}
				}
			}
		}
	}
}

function pythag(a,b) {
	return Math.sqrt(
		(a*a)+
		(b*b)
	);
}

function blog(x) {
	if((game_clock % 60*2 == 0)) {
		console.log(x);
	}
	return x;
}

function myTan2(a,b) {
	return (
				(
					Math.atan2(
						a,
						b
					) *
					180 /
					Math.PI
				) +
				270
			) %
			360;
}
</script>