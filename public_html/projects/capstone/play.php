
<?php 
	include "open_head.php";
	include "db.php";
	include "util.php";
	if(!array_key_exists("user_name", $_SESSION)) {
		//var_dump($_SESSION);
		header('location: login.php');
		exit();
	}
	$arr = [
		"speed",
		"hp",
		"damage"
	];
	if(array_key_exists("hp", $_POST)) {
		foreach($arr as $e) $_SESSION[$e] = $_POST[$e];
		header('location: arena.php');
		exit();
	}
?>
<style>
	* {
		text-shadow: none;
	}
</style>
</head>
<?php
	var_dump($_POST);
?>
<div class="container">
<div class="screen">
<form method="post">
<table>
<tbody>
	<?php
		tableInput("zoom",		"speed", 	"range", 1, 10, 4);
		tableInput("health",    "hp",  		"range", 1, 10, 4);
		tableInput("boom",     	"damage",	"range", 1, 10, 4);
	?>
	<tr>
		<td colspan="2">
			<input type="submit" value="play" style="width:100%;"/>
		</td>
	<tr>
</tbody>
</table>
</form>
<script>	
	var sum = 12;
	var sliders = [4,4,4];
	var a = [
		"speed",
		"hp",
		"damage"
	];
	update();
	//pain(0, a[0], a[1], a[2]);
	function pain(id, master, slave1, slave2) {
		
		$("#"+master).change(
			function(event) {
				$("#span"+master).html($(this).val());
				var diff = $(this).val() - sliders[id];
				console.log(diff);
				$("#"+slave1).val($("#"+slave1).val()+Math.floor(diff/2)+(diff%2==1?(diff>0?1:-1):0));
				$("#"+slave2).val($("#"+slave2).val()+Math.floor(diff/2));
				update(a);
			}
		);
	}
	function update(a) {		
		((Array) (a)).map(e => function(e) {
			$("#span"+e).html($("#"+e).val());
		});
	}
</script>
</div>
</div>