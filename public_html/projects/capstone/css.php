
.screen * {
		text-shadow: 3px 3px black;
	}
        
	body {
		//background-image: url("images/sky-clouds-blue-horizon.jpg");
		background-repeat: no-repeat;
		background-size: cover;
		text-align: left;
		background-color: #96A8C8;
		color: #00FF00;
		font-family: Consolas;
	}
	
	#center {
		text-align: center;
	}
	
	.card {
		border: 3px solid #00FF00;
		display: inline-block;
		list-style: none;
		padding: 0 20px 0 5px;
		box-shadow: 5px 10px black;
		background-color: transparent;
		
	}
	
	nav ul li a, nav ul li:before {
		color: #00FF00;
		text-shadow: 2px 2px black;
	}
	
	
	
	nav ul li:before {
		content: "- ";
	}
