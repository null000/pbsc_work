<?php
	//var_dump($_REQUEST);
	print_r($_REQUEST);
	$us_states = [
	'AL'=>'ALABAMA',
	'AK'=>'ALASKA',
	'AS'=>'AMERICAN SAMOA',
	'AZ'=>'ARIZONA',
	'AR'=>'ARKANSAS',
	'CA'=>'CALIFORNIA',
	'CO'=>'COLORADO',
	'CT'=>'CONNECTICUT',
	'DE'=>'DELAWARE',
	'DC'=>'DISTRICT OF COLUMBIA',
	'FM'=>'FEDERATED STATES OF MICRONESIA',
	'FL'=>'FLORIDA',
	'GA'=>'GEORGIA',
	'GU'=>'GUAM GU',
	'HI'=>'HAWAII',
	'ID'=>'IDAHO',
	'IL'=>'ILLINOIS',
	'IN'=>'INDIANA',
	'IA'=>'IOWA',
	'KS'=>'KANSAS',
	'KY'=>'KENTUCKY',
	'LA'=>'LOUISIANA',
	'ME'=>'MAINE',
	'MH'=>'MARSHALL ISLANDS',
	'MD'=>'MARYLAND',
	'MA'=>'MASSACHUSETTS',
	'MI'=>'MICHIGAN',
	'MN'=>'MINNESOTA',
	'MS'=>'MISSISSIPPI',
	'MO'=>'MISSOURI',
	'MT'=>'MONTANA',
	'NE'=>'NEBRASKA',
	'NV'=>'NEVADA',
	'NH'=>'NEW HAMPSHIRE',
	'NJ'=>'NEW JERSEY',
	'NM'=>'NEW MEXICO',
	'NY'=>'NEW YORK',
	'NC'=>'NORTH CAROLINA',
	'ND'=>'NORTH DAKOTA',
	'MP'=>'NORTHERN MARIANA ISLANDS',
	'OH'=>'OHIO',
	'OK'=>'OKLAHOMA',
	'OR'=>'OREGON',
	'PW'=>'PALAU',
	'PA'=>'PENNSYLVANIA',
	'PR'=>'PUERTO RICO',
	'RI'=>'RHODE ISLAND',
	'SC'=>'SOUTH CAROLINA',
	'SD'=>'SOUTH DAKOTA',
	'TN'=>'TENNESSEE',
	'TX'=>'TEXAS',
	'UT'=>'UTAH',
	'VT'=>'VERMONT',
	'VI'=>'VIRGIN ISLANDS',
	'VA'=>'VIRGINIA',
	'WA'=>'WASHINGTON',
	'WV'=>'WEST VIRGINIA',
	'WI'=>'WISCONSIN',
	'WY'=>'WYOMING'
	];
?>
<p>
	Flavors:
	<?php
	$flavors = ["vanilla","chocolate","strawberry"];
	foreach($flavors as $k => $v) {
			if(array_key_exists($v,  $_REQUEST)) echo "<br/>" . $v;
	}
	?>
</p>
<form method="get">
<table>
<tbody>
	<?php
		tableInput("name",    "name", "text");
	?>
	<tr>
		<td>
			Select state
		</td>
		<td colspan="1">
			<select name="state" style="width:100%">
			<?php
			foreach($us_states as $k => $v) {
			?>
				<option <?php
					if($k == $_REQUEST["state"]) echo "selected=\"selected\"";
							?>value="<?= $k?>"><?= $v ?></option>
							<?php
					}	
					
					?>
			</select>
					</td>
	</tr>
	<tr>
		<td>
			Favorite flavor:
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<input type="submit" style="width:100%;"/>
		</td>
	<tr>
	<?php	
	foreach($flavors as $k => $v) {
		tableInput($v, $v, "checkbox");
	}
	?>
</tbody>
</table>
</form>

<?php
function tableInput($name, $id, $type = "text") {
	?>
	<tr>
		<td><?= $name?></td>
		<td>
			<input type="<?= $type ?>" class="form-control" placeholder="" name="<?= $id?>">
		</td>
	</tr>
	<?php
}
?>
