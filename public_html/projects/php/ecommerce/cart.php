<head>
	<link rel="stylesheet" type="text/css" href="style.css">
	<style>
		thead {
			font-weight: bold;
		}
		#results tfoot td:nth-child(n+2),
		#results tbody td:nth-child(n+4) {
			text-align: right;
		}
		#results tfoot td:nth-child(2)::before,
		#results tbody td:nth-child(2n+2)::before {
			content: "$";
		}
	</style>
</head>
<?php
//my bread and butta
include "util.php";
// Connect to the DB server and select a given DB:
include "db.php";
// Perform the query:
$result = $db->query(
"SELECT inventory.title, inventory.unit_price, cart.in_cart ".
	"FROM `cart` LEFT JOIN inventory ON (cart.sku = inventory.sku) ".
	"WHERE in_cart > 0"
); // E.g. SELECT * FROM inventory
// Fetch the results of the query:
include "nav.php";
?>
<table id="results">
<thead>
	<tr>
	<?=
		td(
		"Item",
		"Unit price",
		"In Cart",
		"Subtotal"
		);
	?>
	</tr>
</thead>
<tbody>
<?php 
	$total = 0;
	while ($row = $result->fetch( PDO::FETCH_ASSOC ))  {
		// Do something with $row, which is an array containing a row of data...
		?><tr><?php
			$subTotal = floatval($row["unit_price"])*floatval($row["in_cart"]);

			echo td(
				[
					$row,
					$subTotal
				]
			);
			$total += $subTotal;
		?><tr><?php
	}
?>
</tbody>
<tfoot>
	<tr>
		<td colspan="3">
			total
		</td>
		<?= 
		//Total recall
		td($total)?>
	</tr>
</tfoot>
</table>