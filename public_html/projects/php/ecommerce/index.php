
<?php
//my bread and butta
include "util.php";
// Connect to the DB server and select a given DB:
include "db.php";
// Perform the query:
	$lookUp = [
		"sku",
		"title",
		"description",
		"unit_price",
		"in_stock",
	];

// Fetch the results of the query:
?>
<head>
	<link rel="stylesheet" type="text/css" href="style.css">
	<style>
		#results tbody td:nth-child(n+4) {
			text-align: right;
		}
		#results tbody td:nth-child(4)::before {
			content: "$";
		}
		<?php
		
			$sort = "sort";
			$order = "order";
			if(!isset($_REQUEST[$sort]))  $_REQUEST[$sort] = 0; 
			if(!isset($_REQUEST[$order])) $_REQUEST[$order] = 1; 

		
			if($_REQUEST[$sort] < 0 || $_REQUEST[$sort] >= sizeof($lookUp) || !is_numeric($_REQUEST[$sort])) { 
			echo 'body {
				background-image: url("sanitize.jpg");
				background-size: contain;
				background-repeat: no-repeat;
			}';
			$_REQUEST[$sort] = $_REQUEST[$sort] % sizeof($lookUp);
			if($_REQUEST[$sort] < 0) $_REQUEST[$sort] = $_REQUEST[$sort] * -1;
			if(!is_numeric($_REQUEST[$sort])) $_REQUEST[$sort] = 0;
			}
		?>
	</style>
</head>
<?php
	$query =  "SELECT * FROM `inventory` ORDER BY ". $lookUp[$_REQUEST[$sort]] . " " . (($_REQUEST[$order]==1)?"ASC":"DESC");
	$result = $db->query($query); // E.g. SELECT * FROM inventory
include "nav.php";
?>
<table id="results">
<thead>
	<tr>
	<?php
		$arr = [
			"Sku",
			"title",
			"description",
			"unit price",
			"in stock"
		];
		foreach ($arr as $k => $v) {
			$temp = "-";
			$bill = 1;
			if($k == $_REQUEST["sort"]) {
				if($_REQUEST[$order] == 1) {
					$temp = "▲";
				}
				else {
					$temp = "▼";
				}
				$bill = -1*($_REQUEST[$order]);
			}
			$arr[$k] = '<a href="index.php?sort='.$k.'&order='.$bill.'">'.$temp."</a>" . $v;
		}
		echo td(
			$arr
		);
	?>
	</tr>
</thead>
<tbody>
<?php 
	while ($row = $result->fetch( PDO::FETCH_ASSOC ))  {
		// Do something with $row, which is an array containing a row of data...
		?><tr><?php
			$sku = array_shift($row);
			echo td(
			"<a href=\"details.php?sku=".$sku."\">".
			$sku
		    ."</a>",
			$row
			);
		?><tr><?php
	}
?>
</tbody>
</table>