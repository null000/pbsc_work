<?php 
//my bread and butta
include "util.php";
// Connect to the DB server and select a given DB:
include "db.php";
// Perform the query:
$result = $db->query( "SELECT * FROM `inventory` WHERE sku=\"".$_REQUEST["sku"].'"'); // E.g. SELECT * FROM inventory
// Fetch the results of the query:
$row = $result->fetch( PDO::FETCH_ASSOC );
?>
<head>
	<link rel="stylesheet" type="text/css" href="style.css">
	<style>
		.h0 {
			font-weight: bold;
			font-size: 1.5rem;
		}
		form {
			border: 1px solid grey;
			display: inline-block;
			padding: .5%;
		}
		.inline-block {
			display: inline-block;
		}
	</style>
	<title>
	<?= $row["title"]?>
	</title>
</head>
<?php 
include "nav.php";
?>
<p>
<div class="inline-block">
	<span class="h0">
		<?= $row["title"]?>
	</span></br>
	<?= $row["description"]?>
</div>
<form>
	Price: $<?= number_format($row["unit_price"],2)?> each</br>
	How many? <input name="quantity" type="number" min="1" placeholder="1" value="1">
	</br>
	<input name="sku" type="hidden" value="<?=$_REQUEST["sku"]?>">
	<input type="submit">
</form>
</p>
<?php 
//update cart
if(isset($_REQUEST["quantity"])) {
	$_REQUEST["quantity"] = floor($_REQUEST["quantity"]);
	if($_REQUEST["quantity"] < 1)                $_REQUEST["quantity"] = 1;
	if($_REQUEST["quantity"] > $row["in_stock"]) $_REQUEST["quantity"] = $row["in_stock"];
	T::setWrapping('"', "");
	$db->query(
	'REPLACE INTO `cart`(`sku`, `in_cart`)'.
	' VALUES ('. td($_REQUEST['sku']) . ','.$_REQUEST["quantity"].')'
	);
}
?>