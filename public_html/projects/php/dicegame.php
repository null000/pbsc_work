<head>
	<style>
		#results, #results th, #results td {
			border: 1px solid black;
			border-collapse: collapse;
		}
		#results td {
			padding:10px;
			margin: 0px;
		}
	</style>
</head>
<?php
	var_dump($_REQUEST);
	class Dice {
		private $face_value;
		private $number_of_sides;
		
		function __construct($number_of_sides = 6) {
			$this->number_of_sides = $number_of_sides;
			$this->roll();
		}
		
		function get_face_value() {
			return $this->face_value;
		}
		
		function roll() {
			$this->face_value = rand(1, $this->number_of_sides);
		}
	}
?>

<form method="get">
<table>
<tbody>
	<?php
		tableInput("player 1", "p0");
		tableInput("player 2", "p1");
		tableInput("number of games", "game_count", "number");
	?>
	<tr>
		<td colspan="2">
			<input type="submit" style="width:100%;"/>
		</td>
	<tr>
</tbody>
</table>
</form>
<?php 
if(isset($_REQUEST["game_count"])) {
if($_REQUEST["p0"] == "") $_REQUEST["p0"] = "Mario";
if($_REQUEST["p1"] == "") $_REQUEST["p1"] = "Lugi";
	?>
<table id="results">
<thead>
	<tr><?=
		td("Roll").
		td($_REQUEST["p0"]." Rolled").
		td($_REQUEST["p1"]." Rolled").
		td("winner");
	?></tr>
</thead>
<tbody>
<?php 
	$d0 = new Dice();
	$d1 = new Dice();
	
	for($i=1; $i <= $_REQUEST["game_count"]; $i++) {
		?><tr><?php	
		$d0->roll();
		$d1->roll();
		echo 
		td($i). //all of this it just works
		td($d0->get_face_value()).
		td($d1->get_face_value());
		$anw = "";
		if($d0->get_face_value() == $d1->get_face_value()) $anw = "Tie";
		if($d0->get_face_value()  > $d1->get_face_value()) $anw = "Player ".$_REQUEST["p0"];
		if($d0->get_face_value()  < $d1->get_face_value()) $anw = "Player ".$_REQUEST["p1"];
		echo td($anw);
		?></tr><?php
	}
?>
</tbody>
</table>
<?php 
}//end if statement
?>
<?php
function td($string) {
	return "<td>" . $string . "</td>";
}
function tableInput($name, $id, $type = "text") {
	?>
	<tr>
		<td><?= $name?></td>
		<td>
			<input type="<?= $type ?>" class="form-control" placeholder="" name="<?= $id?>">
		</td>
	</tr>
	<?php
}
?>
