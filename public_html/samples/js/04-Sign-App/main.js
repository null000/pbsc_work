// ---------------------------
// Validate form
// ---------------------------
// How much would it cost for your name to be converted to a sign
// Each letter and special characters cost 5 dollars, you should be
// taxed on the product and not shipping, shipping comes in 3 flavors,
// and you get a small choice of different fonts ( no extra charge ).
//
// Take everything you have learned form the past examples to execute the app.
//change to 0 if u want real time update
var mode = 1;

// Caching all DOM Input Elements necessary
var nameDisplay 		= document.querySelector('#name');
var nameInput 			= document.querySelector('[name="name"]');
var fontInput 			= document.querySelector('[name="font-select"]');

var specialCharsInput 	= document.querySelector('[name="specialChars"]');
	if(mode === 0) specialCharsInput.addEventListener("input", calculateCosts);

var shippingInput 		= document.querySelector('[name="shipping"]');
	if(mode === 0) shippingInput.addEventListener("input", calculateCosts);
document.querySelector("#calculate").addEventListener("click", calculateCosts);
// Cache all DOM Display Elements here
var display 			= document.querySelectorAll(".cost-detail > span");
console.log(display);
// Business logic
var costPerLetter = 5;
var tax = .06;

nameInput.addEventListener('input', function(e) {
  // if the user has entered a value in the nameInput, then display it
  // if not, then display 'Your Name Here'
  if (e.target.value) {
    nameDisplay.innerHTML = e.target.value;
  } else {
    nameDisplay.innerHTML = 'Your Name Here';
  }
  if(mode === 0)calculateCosts(e);
});

// Listening to the fontInput drop down for a change.
// when it changes, change the font family style of the name display
fontInput.addEventListener('change', function(e) {
  nameDisplay.style.fontFamily = e.target.value;
});


function calculateCosts(e) {
  e.preventDefault();
  // do all your business logic here
  var count = 0;
  for(var i = 0; i < nameInput.value.length; i++) {
	  if(nameInput.value.charAt(i) !== " ") {
		  count++;
	  }
  }
  console.log((~0>>>1));
  var array = [
	parseFloat((count * costPerLetter)),
	//							magic magic magic
	parseFloat(
		//javascript casting implementation
		//((specialCharsInput.value>0)*2-1)
		//pure math on 64 bit float hacking todo fix
		-((specialCharsInput.value>>>63<<1)-1)
		*
		specialCharsInput.value
		*
		costPerLetter
	),
	2,
	3,
	4,
	5
  ];
  array[2] = parseFloat((array[0]+array[1])*tax);
  array[3] = parseFloat(shippingInput.options[shippingInput.selectedIndex].value);
  array[4] = parseFloat((array[0]+array[1])+array[3]);
  array[5] = parseFloat(array[2]+array[4]);
  for(var i = 0; i < display.length; i++)
  display[i].innerHTML = "$"+array[i].toFixed(2);

}
//console.log(document.querySelector('body').innerHTML);
//console.log(document.querySelector('body').innerTEXT);


class Food {
	constructor(name, cost) {
		this.name = name;
		this.cost = cost;
	}
	toString() {
		return "I am " + this.name + " I cost $" + this.cost.toFixed(2);
	}
}
var cart = [
	new Food("bread",  1.29),
	new Food("milk",   1.33),
	new Food("cheese", 2.40)
];
var grand_total = 0;

for(var i in cart) {
	console.log(cart[i].toString());
	console.log(i);
//look in ur cart and add the cost to grand total
	grand_total += cart[i].cost;
}

console.log("the grand_total is " + grand_total);