// ---------------------------
// Validate form
// ---------------------------

/*

When the user submits the form, we want the info from that form
(first name, last name, Twitter handle) to display in the h1.displayName element.

You will need to do the following two things:

1. Add an event listener to the form so that when the form is submitted, it calls the function named displayName().

2. Add code to the displayName() function so it does the following:

	a) Retrieve the values that the user typed into the fname, lname, and twitterHandle fields
	b) Show those values in the h1.displayName element by changing its .innerHTML property

BONUS: If either the first name, last name, or Twitter handle are blank, don't display them.
       Instead, display a suitable error message telling the user that the need to enter the missing data.

*/

// 1. ADD YOUR EVENT LISTENER HERE:
document.querySelector("#submit").addEventListener("click", displayName);
console.log((0/0).toString() === "NaN");
console.log("0/0 !== 0/0 "+(0/0 !== 0/0));
console.log("0/0 === 0/0 "+(0/0 === 0/0));

console.log(6 !== 0/0);
console.log(6 === 0/0);
function displayName(e){
	
	e.preventDefault(); // This prevents the form from submitting

	// 2. PROVIDE CODE BELOW TO DO THE FOLLOWING:

	// Retrieve the first name:
	var list = document.querySelectorAll("input.form-control");
	console.log(list);
	
	// Retrieve the last name:

	// Retrieve the Twitter handle:
	var temp = "";
	// Display first name, last name, and Twitter handle in the displayName element:
	for(var i = 0; i < list.length; i++) {
		if(list[i].value === null || list[i].value === "") {
			alert("one of these things is not like the other");
			return null;
		}
		temp += list[i].value + " ";
	}
	document.querySelector(".displayName").innerHTML = temp;
}
