// ---------------------------
// Display Images
// ---------------------------
// Display the corresponding image when the button is clicked

// Cache each button, attach event listener to trigger imgToggle

// Do the same thing below for the other buttons:

// ADD YOUR CODE HERE!
//////////////////////////////////////////////////////////////////////////////////////////
// [ Bonus ]
// Instead of creating a variable for each item on the screen, select them all at once and add an event listener to each one in a loop.
//////////////////////////////////////////////////////////////////////////////////////////
var list = document.querySelectorAll("button");
for(var i = 0; i < list.length; i++) 
	list[i].addEventListener("click", imgToggle);





function imgToggle(e) {
	var pic = document.getElementById( this.className );

	// check to see if the element has a class of 'isHidden'
	if (pic.classList.contains('isHidden')) {
		// remove it
		pic.classList.remove('isHidden');
	} else {
		pic.classList.add('isHidden');
		// add it
	}
}


