// ---------------------------
// Arithmetics
// ---------------------------
// Create variables and functions necessary to add, subtract, multiply, and divide two numbers together.
// You are going to need to select the DOM elements, listen for events, and run some code based on that event being triggered.


////////////////////////////////////////////////////////////////////////////////
// 1. Add the two numbers from input fields 'num-one' and 'num-two':

// This sets up an event handler to invoke the add() function when the user types in the num-one field:
document.querySelector('.num-one').addEventListener('input', add);

// ADD YOUR CODE HERE to set up an event listener for num-two so it triggers the same event:
document.querySelector('.num-two').addEventListener('input', add);

document.querySelector('.sub-one').addEventListener('input', subtract);
document.querySelector('.sub-two').addEventListener('input', subtract);

document.querySelector('.mult-one').addEventListener('input', mult);
document.querySelector('.mult-two').addEventListener('input', mult);

document.querySelector('.div-one').addEventListener('input', div);
document.querySelector('.div-two').addEventListener('input', div);

document.querySelector('.calc-one').addEventListener('input', calc);
document.querySelector('.calc-two').addEventListener('input', calc);
document.querySelector('.op').addEventListener('input', calc);

function add() {
	var one = document.querySelector('.num-one').value;
	var two = document.querySelector('.num-two').value;
	document.querySelector('.add-sum').innerHTML = 
	parseInt(one) +
	parseInt(two);
	// ADD YOUR CODE HERE to display the result in the add-sum DOM element by changing its .innerHTML property:
}



////////////////////////////////////////////////////////////////////////////////
// 2. Subtract the two numbers from input fields 'sub-one' and 'sub-two'

// Set up event listeners that listen for the input change, and invoke the subtract() function when triggered

function subtract() {
	// Get the value out of the first input field from the variable created above
	// Get the value out of the second input field from the variable created above
	var one = document.querySelector('.sub-one').value;
	var two = document.querySelector('.sub-two').value;
	document.querySelector('.sub-sum').innerHTML = 
	parseInt(one) -
	parseInt(two);
	// Display the result in the display DOM element
}

////////////////////////////////////////////////////////////////////////////////
// 3. Multiply the two numbers from input fields 'mult-one' and 'mult-two'
function mult() {
	// Get the value out of the first input field from the variable created above
	// Get the value out of the second input field from the variable created above
	var one = document.querySelector('.mult-one').value;
	var two = document.querySelector('.mult-two').value;
	document.querySelector('.mult-sum').innerHTML = 
	parseInt(one) *
	parseInt(two);
	// Display the result in the display DOM element
}
// Set up event listeners that listen for the input change, and invoke the multiply() function when triggered

// Create Function here

// Set up event listeners that listen for the input change, and invoke the mult() function when triggered


////////////////////////////////////////////////////////////////////////////////
// 4. Divide the two numbers from input fields 'div-one' and 'div-two'
function div() {
	// Get the value out of the first input field from the variable created above
	// Get the value out of the second input field from the variable created above
	var one = document.querySelector('.div-one').value;
	var two = document.querySelector('.div-two').value;
	document.querySelector('.div-sum').innerHTML = 
	parseInt(one) /
	parseInt(two);
	// Display the result in the display DOM element
}
// Set up event listeners that listen for the input change, and invoke the multiply() function when triggered

// Create Function here

// Set up event listeners that listen for the input change, and invoke the divide() function when triggered


////////////////////////////////////////////////////////////////////////////////
// 5. [ Bonus ]
// Create the program that can accept the two numbers, as well as the operator desired.
// Hint - You will need to create the HTML markup for these inputs
function calc() {
	// Get the value out of the first input field from the variable created above
	// Get the value out of the second input field from the variable created above
	var one = parseInt(document.querySelector('.calc-one').value);
	var two = parseInt(document.querySelector('.calc-two').value);
	var operator = document.querySelector('.op').value;
	var anw = mySwitch;
	document.querySelector('.calc-sum').innerHTML = mySwitch(one, two, operator);
	// Display the result in the display DOM element
}
function mySwitch(one, two, operator) {
	switch(operator) {
		case "-": return one - two;
		case "+": return one + two;
		case "*": return one * two;
		case "/": return one / two;
		case "": return "select an operator";
		default: return "not a valid operator";
	}
}
////////////////////////////////////////////////////////////////////////////////
