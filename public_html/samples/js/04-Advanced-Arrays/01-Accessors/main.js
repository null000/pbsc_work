// ---------------------------
// Accessor Methods
// ---------------------------
// These methods are all about letting you know whats in your array.
var characters = ['Fry', 'Leela', 'Amy', 'Bender', 'Professor', 'Zapp', 'Zoidberg', 'Kif'];
function check(name) {
	alert(name + " is at position " + characters.indexOf(name));
	alert(name + " is"+(characters.indexOf(name)==-1?" NOT":"") + " in the array");
}
////////////////////////////////////////////////////////
// 1. Where is Bender in the array?
// Alert the user what index he can be found at:
// "Bender is at position [n]"

////////////////////////////////////////////////////////
// 2. Determine whether Bender is in the array.
// Alert the user with a message that tells whether Bender is (or isn't!) in the array:
// "Bender is in the array" or "Bender is NOT in the array"
check("Bender");

////////////////////////////////////////////////////////
// 1. Where is Scruffy in the array?
// Alert the user what index he can be found at:
// "Scruffy is at position [n]"

////////////////////////////////////////////////////////
// 4. Determine whether Scruffy is in the array.
// Alert the user with a message that tells whether Scruffy is (or isn't!) in the array.
// "Scruffy is in the array" or "Scruffy is NOT in the array"
check("Scruffy");
