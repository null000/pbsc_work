// ---------------------------
// Mutator Methods
// ---------------------------
// This exercise is all about altering an array.
var characters = ['Fry', 'Leela', 'Bender'];

////////////////////////////////////////////////////////
// 1. Add 'Professor' and 'Amy' to the end of the array. Log the contents of the array before and after you alter it, so you can prove it worked.
var i = 1;
console.log(i++);
console.log(characters.join(" "));
characters.push("Professor");
characters.push("Amy");
console.log(characters.join(" "));

////////////////////////////////////////////////////////
// 2. Remove the last value off the end of the array. Log the contents of the array before and after you alter it, so you can prove it worked.
console.log(i++);
console.log(characters.join(" "));
characters.pop();
console.log(characters.join(" "));

////////////////////////////////////////////////////////
// 3. Remove 'Bender' and ' Professor' from the array. Log the contents of the array before and after you alter it, so you can prove it worked.
console.log(i++);
console.log(characters.join(" "));
characters.splice(characters.indexOf("Bender"),1);
characters.splice(characters.indexOf("Professor"),1);
console.log(characters.join(" "));

////////////////////////////////////////////////////////
// 4. Add 'Bender', 'Professor', and 'Zoidberg' into the array. Log the contents of the array before and after you alter it, so you can prove it worked.
console.log(i++);
console.log(characters.join(" "));
characters.push("Bender");
characters.push("Professor");
characters.push("Zoidberg");
console.log(characters.join(" "));

////////////////////////////////////////////////////////
// 5. Sort the array alphabetically by name. Log the contents of the array before and after you alter it, so you can prove it worked.
console.log(i++);
console.log(characters.join(" "));
characters.sort();
console.log(characters.join(" "));
//randomizer
var swap1, swap2, temp;
for(var i = 0; i < 100; i++) {
	swap1 = Math.floor(Math.random() * characters.length);
	swap2 = Math.floor(Math.random() * characters.length);
	temp = characters[swap1];
	characters[swap1] = characters[swap2];
	characters[swap2] = temp;
}
console.log(characters.join(" "));
//todo not done
function mergeSort(a[], b[]) {
	var sum = a.length + b.length; 
	var anw = new Array(sum);
	var ai = 0;
	var bi = 0;
	for (var i = 0; i < sum; i++) {
		if(!(ai < a.length)) {
			
		}
		if(!(bi < b.length)) {
			
		}
	}
	return anw;
	
}